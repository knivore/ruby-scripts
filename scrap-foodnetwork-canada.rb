#!/usr/bin/ruby
require 'open-uri'
require 'selenium-webdriver'
require 'mysql'
require 'open-uri'
require 'net/http'
require 'net/scp'
require 'net/sftp'
require 'securerandom'

## Set current development environment (local, production)
environment = ARGV[0]
## Get url to scrap if any
urlToScrap = ARGV[1]
## Get particular url to scrap if any (True or False)
particularUrlToScrap = false
if ARGV[2].to_s != ""
	particularUrlToScrap = ARGV[2] 
end

## Display Help Menu
if environment.to_s.casecmp("help") == 0
	puts "This ruby script looks into foodnetwork-ca.com and scrap their recipes based on the following 3 parameters."
	puts "1. Environment - Local or Prod (Required)"
	puts "2. The URL you wish to scrap (Optional)"
	puts "3. The URL is a particular recipe url page (True or False) (Optional)"
	exit
end

## Check environment input
if environment.to_s.casecmp("local") != 0 && environment.to_s.casecmp("prod") != 0 
	puts "Environment entered must be local or prod"
	exit
end

page1RecipesHref = []
wait = Selenium::WebDriver::Wait.new(:timeout => 30)
driver = Selenium::WebDriver.for :chrome
driver.manage.timeouts.page_load = 300 ## 5 minutes9
driver.manage.timeouts.implicit_wait = 40 ## retry for maximum 40 seconds before throwing an exception
if urlToScrap.to_s == ""
	driver.navigate.to "http://www.foodnetwork.ca/everyday-cooking/recipes/?fspn=3"
	http = Net::HTTP.new("http://www.foodnetwork.ca/everyday-cooking/recipes/?fspn=3")
 else 
 	driver.navigate.to "#{urlToScrap}"
 	http = Net::HTTP.new("#{urlToScrap}")
 end
http.read_timeout = 300

if particularUrlToScrap.to_s.casecmp("false") == 0 || particularUrlToScrap.to_s == ""
	#my = Mysql.new(hostname, username, password, databasename)
	if environment.to_s == "local"
		con = Mysql.new('localhost', 'root', '', 'foodDB')
	else
		con = Mysql.new('54.169.116.93', 'foodtopic', 'foodtopic@123', 'foodDB')
	end

	## Loop thru all recipes on 1st page
	allElements = wait.until { driver.find_elements(:xpath, "//*[@class='searchResultsEntry']") }
	allElements.each do |element|
		recipeName = element.find_element(:class, 'searchResultsEntry-title').text
		recipeHref = element.find_element(:class, 'searchResultsEntry-titleLink').attribute("href")
		
		## Check if query of recipe title exists in our DB
		results = con.query("SELECT * FROM RECIPE WHERE RECIPE_NAME = '" + Mysql.escape_string(recipeName) + "'")
		if results.num_rows == 0
			#puts recipeName
			page1RecipesHref.push(recipeHref)
		end
	end

	con.close
else 
	page1RecipesHref.push("#{urlToScrap}")
end

page1RecipesHref.each do |href|
	driver.navigate.to href
	http = Net::HTTP.new("#{href}")
	http.read_timeout = 500

	if environment.to_s.casecmp("local") == 0
		userId = "1"
	else
		userId = Random.rand(3...11)
	end
	
	recipe_contains_multiple_recipes = wait.until { driver.find_elements(:xpath, "//strong[@class='recipe-setTitle']") }
	## If recipe does not contains multiple recipes to make
	if recipe_contains_multiple_recipes.size() == 0 || particularUrlToScrap.to_s.casecmp("true") == 0

		recipeName = wait.until { driver.find_element(:xpath, "//*[@class='recipeTitle']").text }
	    puts "Starting insertion of #{recipeName}..."
		#puts "Recipe Name: #{recipeName}"
		prepTime = driver.find_element(:xpath, "//*[@itemprop='prepTime']").text
		if prepTime.to_s == ""
			prepTime = "0"
		end
		#puts "Recipe Preparation Time: #{prepTime}"
		totalTime = driver.find_element(:xpath, "//*[@itemprop='totalTime']").text
		if totalTime.to_s == ""
			totalTime = "0"
		end
		#puts "Recipe Total Time: #{totalTime}"
		serves = driver.find_element(:xpath, "//*[@itemprop='recipeYield']/span[2]/span").text
		if serves.to_s == ""
			serves = "1"
		end
		#puts "Recipe Servings: #{serves}"
		recipeDescription = driver.find_element(:xpath, "//*[@class='leftCol-recipeDescription']").text
		#puts "Recipe Description: #{recipeDescription}"

		begin
			recipeTipsSubstitutions = driver.find_element(:xpath, "//div[contains(@class, 'recipeDetails-tipsAndSubstitutions')]")
			if recipeTipsSubstitutions.to_s != ""
				recipeDescription = recipeDescription + recipeTipsSubstitutions.text
				#puts "Tips & Substitutions: #{recipeTipsSubstitutions.text}"
			end
		rescue
		    puts "No Recipe Tips & Substitutions"
		end

		begin
			recipeSourceCredits = driver.find_elements(:xpath, "//div[contains(@class, 'recipeDetails-sourceAndCredits')]//p")
			if recipeSourceCredits.size() > 0
				recipeDescription = "#{recipeDescription} Credits to foodnetwork.ca - #{recipeSourceCredits}"
				#puts "foodnetwork.ca - #{recipeSourceCredits}"
			#else 
			#	recipeDescription = "#{recipeDescription} - Credits to foodnetwork.ca"
			end
		rescue
		    puts "No Source & Credits"
		end

		begin
			currentDateTime = Time.now.strftime("%Y-%m-%d %H:%M:%S")

			if environment.to_s.casecmp("local") == 0
				con = Mysql.new('localhost', 'root', '', 'foodDB')
			else
				con = Mysql.new('54.169.116.93', 'foodtopic', 'foodtopic@123', 'foodDB')
			end
			con.autocommit false

			con.query("INSERT INTO RECIPE(USER_ID, RECIPE_NAME, RECIPE_DESCRIPTION, PREPARATION_TIME, SERVINGS, MONETIZATION_CODE, STATUS_CODE, PRIVACY_CODE, DIFFICULTY_CODE, COOK_FOR_ME, VIEW_COUNT, PREVIOUS_RECIPE_ID, CREATED_DATE, UPDATED_DATE, VERSION, PUBLISHED_DATE) 
				 			 	   VALUES(#{userId}, '" + Mysql.escape_string(recipeName[0...255]) + "', '" + Mysql.escape_string(recipeDescription[0...255]) + "', " + Mysql.escape_string(prepTime.gsub(/\s.+/, '')) + ", " + Mysql.escape_string(serves.gsub(/\s.+/, '')) + ", 5, 8, 13, 27, null, 0, null, now(), now(), 1, now())")
			recipeId = con.insert_id
			#puts "Recipe #{recipeId} created!"
			
			## Loop thru all recipe ingredients and insert
			recipeIngredients = wait.until { driver.find_elements(:xpath, "//*[@class='recipe-ingredients']//p") }
			recipeIngredients.each_with_index do |recipeIngredient, index|
				recipeIngredientName = recipeIngredient.text
				if recipeIngredientName.include? "⅛"
					recipeIngredientName.gsub! '⅛', '1/8'
				end
				if recipeIngredientName.include? "⅓"
					recipeIngredientName.gsub! '⅓', '1/3'
				end
				if recipeIngredientName.include? "⅔"
					recipeIngredientName.gsub! '⅔', '2/3'
				end
				if recipeIngredientName.include? "½"
					recipeIngredientName.gsub! '½', '1/2'
				end
				if recipeIngredientName.include? "¾"
					recipeIngredientName.gsub! '¾', '3/4'
				end
				if recipeIngredientName.include? "¼"
					recipeIngredientName.gsub! '¼', '1/4'
				end
				#puts "Recipe Ingredient #{index}: #{recipeIngredientName}"
				con.query("INSERT INTO RECIPE_INGREDIENT(RECIPE_ID, INGREDIENT_NAME, INGREDIENT_QUANTITY, INGREDIENT_UNIT, CREATED_DATE, UPDATED_DATE, VERSION) 
						 		       VALUES(#{recipeId}, '" + Mysql.escape_string(recipeIngredientName[0...255]) + "', '', '', now(), now(), 1)")
			end

			## Loop thru all recipe directions and insert
			recipeDirections = wait.until { driver.find_elements(:xpath, "//*[@class='recipeInstructions recipeDescriptionText']//p") }
			recipeDirections.each_with_index do |direction, index|
				recipeDirection = direction.text
				if recipeDirection.include? "½"
					recipeDirection.gsub! '½', '1/2'
				end
				if recipeDirection.include? "−"
					recipeDirection.gsub! '−', ''
				end
				
				recipeDirection = recipeDirection[3..recipeDirection.length]
				#puts "Recipe Direction #{index}: #{recipeDirection}"
				con.query("INSERT INTO RECIPE_DIRECTION(RECIPE_ID, DIRECTION_DESCRIPTION, DIRECTION_IMAGE_PATH, CREATED_DATE, UPDATED_DATE, VERSION) 
			 			 		       VALUES(#{recipeId}, '" + Mysql.escape_string(recipeDirection[0...255]) + "', null, now(), now(), 1)")
			end

			## Download Recipe Image and place into server path
			recipeImageHref = driver.find_element(:xpath, "//*[@class='recipe-figure']/img").attribute("src")
			recipeImageName = SecureRandom.urlsafe_base64(36)

			recipeImagePath = "/Users/K/Documents/#{recipeImageName}.jpg"
			recipeServerImagePath = "/ext/recipes/#{recipeId}/coverPhotos/#{recipeImageName}.jpg"
			#puts "Recipe Image URL: #{recipeImageHref}"
			if recipeImageHref != ""
				File.open("#{recipeImagePath}", 'wb') do |fo|
					fo.write open("#{recipeImageHref}").read 
				end
			end
			if environment.to_s.casecmp("local") != 0
				Net::SFTP.start("ec2-54-169-116-93.ap-southeast-1.compute.amazonaws.com", "ec2-user", :keys => [ "~/.ssh/keh" ]) do |sftp|
					sftp.mkdir!("/home/ec2-user/foodtopic/foodtopic-1.0-SNAPSHOT/ext/recipes/#{recipeId}")
					sftp.mkdir!("/home/ec2-user/foodtopic/foodtopic-1.0-SNAPSHOT/ext/recipes/#{recipeId}/coverPhotos")
					sftp.upload!("#{recipeImagePath}", "/home/ec2-user/foodtopic/foodtopic-1.0-SNAPSHOT#{recipeServerImagePath}")
				end
			end
			con.query("INSERT INTO RECIPE_IMAGE(RECIPE_ID, RECIPE_IMAGE_NAME, RECIPE_IMAGE_PATH, CREATED_DATE, UPDATED_DATE, VERSION) 
								   VALUES(#{recipeId}, '#{recipeImageName}', '#{recipeServerImagePath}', now(), now(), 1)")

			## Update user reputation points to be in sync
			userResult = con.query("SELECT * FROM USER WHERE USER_ID = #{userId}")
			userFields = userResult.fetch_hash
			updatedReputationPoints = userFields["REPUTATION_POINTS"].to_i
			updatedReputationPoints += 10
			con.query("UPDATE USER SET REPUTATION_POINTS = #{updatedReputationPoints} WHERE USER_ID = #{userId}")
			
			con.commit 

		    puts "Insert #{recipeName} into DB Successfully!"
		rescue Mysql::Error => e
		    puts e.errno
		    puts e.error
		    File.delete(recipeImagePath) if File.exist?(recipeImagePath)
		    puts "Insert #{recipeName} into DB Failed!"
		ensure
		    con.close if con
		end
	end
end