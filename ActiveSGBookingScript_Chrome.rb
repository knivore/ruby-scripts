#!/usr/bin/ruby
require 'selenium-webdriver'
require 'date'
require 'time'
require 'net/smtp'
require 'twilio-ruby'

# Default Variables
maxBookingOnActiveSG = 2
account_sid = 'ACd8ba2ac1d61947285cadbe48128c6213'
auth_token = '0a14aabea069d39741b7f96fea046d93'
defaultSmsSender = "+18133773436"

# set up a client to talk to the Twilio REST API
@client = Twilio::REST::Client.new account_sid, auth_token 
@driver = Selenium::WebDriver.for :chrome

tries = 3
begin
	# Accept user inputs
	if ARGV[0].to_s != ""
		day = ARGV[0]
	else
		puts "Day, Time & Duration Required. Min 3 params."
		exit 1
	end

	if ARGV[1].to_s != ""
		time = ARGV[1]
	else
		puts "Day, Time & Duration Required. Min 3 params."
		exit 1
	end

	if ARGV[2].to_s != ""
		duration = ARGV[2]
	else
		puts "Day, Time & Duration Required. Min 3 params."
		exit 1
	end

	venue = "850"
	if ARGV[3].to_s != ""
		venue = ARGV[3] 
	end

	activity = "18"
	if ARGV[4].to_s != ""
		activity = ARGV[4]
	end

	nric = "S8942892J"
	if ARGV[5].to_s != ""
		nric = ARGV[5] 
	end

	password = "pancake89"
	if ARGV[6].to_s != ""
		password = ARGV[6] 
	end

	inputDate = Date.today + 15
	if ARGV[7].to_s != ""
		inputDate = ARGV[7]
	end

	## Display Help Menu
	if ARGV[0].to_s.casecmp("help") == 0
		puts "This ruby script looks into activesg and find the activity & location you wish to make a booking with"
		puts "1. Day (0 = Sunday, 1 = Monday, 2 = Tuesday, 3 = Wednesday, 4 = Thursday, 5 = Friday, 6 = Saturday)  *REQUIRED*"
		puts "2. Time (HH:mm)  *REQUIRED*"
		puts "3. Duration  *REQUIRED*"
		puts "4. Venue (291 = Bedok Sports Hall, 542 = Pasir Ris Sports Hall, 850 = Tampines Hub)"
		puts "5. Activity (18 = Badminton - Default if empty)"
		puts "6. NRIC (The NRIC Account to login to Active SG)"
		puts "7. Password (The Account Password to login to Active SG)"
		puts "8. Date (yyyy-MM-dd)"
		exit
	end

	@driver.manage().window().maximize();
	@driver.manage.timeouts.implicit_wait = 20 ## retry for maximum 20 seconds before throwing an exception
	@driver.navigate.to "https://members.myactivesg.com/auth"

	element = @driver.find_element(:name, 'email')
	element.send_keys nric
	element = @driver.find_element(:name, 'password')
	element.send_keys password

	## Click on Submit Button
	@driver.find_element(:id, 'btn-submit-login').click

	## Check if login failed
	begin
		loginFailureText = @driver.find_element(:xpath, '//div[contains(@class, "alert alert-danger")]').text
		if loginFailureText.include?"Invalid user name or password."
			puts "Invalid Login. Please re-run with a valid login"
			tries = 0
			@driver.quit()
			exit 1
		end
	rescue
	end

	## Navigate to URL
	@driver.navigate.to "https://members.myactivesg.com/facilities/view/activity/" + activity + "/venue/" + venue

  	date_calendar = @driver.find_element(:class, 'datepicker')
	@driver.execute_script("arguments[0].scrollIntoView(true);", date_calendar)
	date_calendar.click

	active_date_element = @driver.find_element(:xpath, '//*[contains(@class, "ui-datepicker-current-day")]')
	active_year = active_date_element.attribute("data-year")
	active_month = 1 + active_date_element.attribute("data-month").to_i
	active_day = @driver.find_element(:xpath, '//*[contains(@class, "ui-state-default ui-state-active")]').text
	active_date = Date.parse(active_year + "-" + active_month.to_s + "-" + active_day)

	puts "Earliest available date is #{active_date}"

	## If never enter a particular date
	if ARGV[7].to_s == ""
		## Check if the day entered is the same as the latest opening date
		if active_date.wday != day.to_i
			puts "Its not " + Date::DAYNAMES[day.to_i] + " on " + active_date.to_s
			tries = 0
			@driver.quit()
			exit 1
		end
	else
		begin
			inputDate = Date.parse(inputDate)
		rescue
			puts "Wrong input of date. Exiting now..."
			tries = 0
			@driver.quit()
			exit 1
		end

		if inputDate > active_date
			puts "Inputted Date is beyond the earliest date open on ActiveSG website"
		else
			##TODO: Haven figure out...
		end
	end

	numberOfCourts = @driver.find_elements(:xpath, "//h4[contains(@class, 'fac-court-name')]")
	puts "Number of Courts at : #{numberOfCourts.size} "

	court = 1
	count = 0
	for i in 0..numberOfCourts.size-1
		puts "Searching court #{i+1} with duration of #{duration} hours from #{time}..."
		if duration.to_i > 1
			for t in 0..duration.to_i-1
				parseTime = Time.parse(time) + (60 * 60 * t)
				nextTiming = @driver.find_element(:xpath, "//div[contains(@class, 'subvenue-slot')][#{i+1}]/div/div[contains(., '#{parseTime.strftime("%R")}')]")
				if !nextTiming.attribute("for").to_s.empty? && count < maxBookingOnActiveSG
					@driver.action.move_to(nextTiming).click.perform
					count += 1
				else
					break
				end
			end
		else
			startTiming = @driver.find_element(:xpath, "//div[contains(@class, 'subvenue-slot')][#{i+1}]/div/div[contains(., '#{time}')]")
			if !startTiming.attribute("for").to_s.empty? && count < maxBookingOnActiveSG
				@driver.action.move_to(startTiming).click.perform
				count += 1
			else
				break
			end
		end
		court = i
	end

	puts case venue
	when "850"
		venue = "Tampines Hub"
	when "542"
		venue = "Pasir Ris Sports Hall"
	when "291"
		venue = "Bedok Sports Hall"
	else

	end

	if count > 0
		@driver.find_element(:id, 'paynow').click
		@driver.switch_to.alert.accept

		puts "There is a #{duration} hours slot from #{time} at #{venue} court #{court}. You have approx 9m30s to decide and login to complete the purchase before the shopping cart expires."

		@client.account.messages.create(
		  from: defaultSmsSender,
		  to: '+6590682473',
		  body: "There is a #{duration} hours slot from #{time} at #{venue} court #{court}. You have approx 9m30s to decide and login to complete the purchase before the shopping cart expires."
		)

		@client.account.messages.create(
			from: defaultSmsSender,
			to: '+6583338228',
			body: "There is a #{duration} hours slot from #{time} at #{venue} court #{court}. You have approx 9m30s to decide and login to complete the purchase before the shopping cart expires."
		)
	else
		puts "There are no #{venue} courts available with duration of #{duration} hours from #{time}. Please re-run program with another timing."
		
		@client.account.messages.create(
		  from: defaultSmsSender,
		  to: '+6590682473',
		  body: "There are no #{venue} courts available with duration of #{duration} hours from #{time}. Please re-run program with another timing."
		)

		@client.account.messages.create(
		  from: defaultSmsSender,
		  to: '+6583338228',
		  body: "There are no #{venue} courts available with duration of #{duration} hours from #{time}. Please re-run program with another timing."
		)
	end
rescue Exception => ex
  	puts "An error of type #{ex.class} happened, message is #{ex.message}"

	tries -= 1
  	if tries > 0
  		puts "Retrying... #{tries} times left..."
		@driver.quit()
    	retry
    else
		@driver.quit()
		exit 1
  	end
end

@driver.quit()
exit 1