require 'fileutils'

Dir.glob("./*.png") do |file_name|
  if !File.directory? file_name
    directory_name = File.basename(file_name, '.png')
    Dir.mkdir(directory_name) unless File.exists?(directory_name)

    destination = Dir.pwd + "/#{directory_name}/#{file_name}"
    FileUtils.move(Dir.pwd + "/#{file_name}", Dir.pwd + "/#{directory_name}/#{file_name}")
    
    File.rename(Dir.pwd + "/#{directory_name}/#{file_name}", Dir.pwd + "/#{directory_name}/icon-transparent-336x90.png")
    puts "Move & Rename " + file_name + " to icon-transparent-336x90.png successfully"
  end
end